package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.Event;
	import com.eclecticdesignstudio.motion.Actuate;
	import flash.events.KeyboardEvent;
	
	import flash.ui.Mouse;
	
	import DiscordlingsGame;
	import Game;
	import Menu;
	import MenuEvent;
	
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 * 
	 */
	
	public class Main extends MovieClip {
		
		public var intro:Intro;
		public var mainmenu:MainMenu;
		public var game:DiscordlingsGame;
		
		private var menues:Array;
		
		private const basicBG:Bitmap = new Bitmap(new BitmapData(stage.stageWidth, stage.stageHeight, true, 0xFFFFFFFF), "auto", true);
		
		public function init(e:Event = null):void {
			if (e) removeEventListener(Event.ADDED_TO_STAGE, init);
			
			intro = new Intro();
			intro.set_bg(basicBG);
			intro.x = intro.y = 0;
			intro.addEventListener(Event.COMPLETE, introComplete);
			
			mainmenu = new MainMenu(1920, 1080);
			mainmenu.x = 75, mainmenu.y = -35;
			//mainmenu.addEventListener("click", menuHandler, false, 0, true);
			mainmenu.addEventListener(MenuEvent.SELECT, menuHandler, false, 0, true);
			mainmenu.addEventListener(MenuEvent.DONE, menuHandler, false, 0, true);
			
			game = new DiscordlingsGame(stage.stageWidth, stage.stageHeight, null, 30);
			game.x = (stage.stageWidth - game.width) / 2;
			game.y = (stage.stageHeight - game.height) / 2;
			game.alpha = 0;
			
			game.addEventListener(Game.COMPLETE, gameOver);	// game ends with end-of-stage
			game.addEventListener(Game.HALT, gameOver);		// game ends unexpectedly.. not sure if possible to event
			game.addEventListener(Game.STOP, gameOver);		// game stopped by user (i.e. "quit" in a level)
			
			this.addChild(game);
			this.addChild(mainmenu.hide());
			this.addChild(intro);
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, game.keyboardInput);
			stage.addEventListener(KeyboardEvent.KEY_UP, game.keyboardInput);
			
			intro.play();
		}
		
		private function introComplete(e:Event = null):void {
			Actuate.tween(intro, 2, { alpha: 0 }, false).onComplete(function intoComplete():void {
				intro.hide();
				Actuate.tween(mainmenu, 2, { alpha:1, x:0, y:0 } );
			});
		}
		
		private function menuHandler(e:Event):void {
			var ty:String = e.type;
			if (e.type === MenuEvent.DONE) {
				game.playlist = mainmenu.playlist;
				Actuate.tween(mainmenu, 1, { alpha: 0 } ).onComplete(function gameFadein():void {
					mainmenu.hide();
					Actuate.tween(game, 1, { alpha:1, x:0, y:0 } );
					game.totalLength = mainmenu.playlistLength;
					game.start();
				});
			};
		}
		
		private function gameOver(e:Event = null):void {
			trace(e);
		}
		
		public function Main() {
			super();
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
	}
}