package  
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.media.Sound;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import com.eclecticdesignstudio.motion.Actuate;
	import com.eclecticdesignstudio.motion.easing.Elastic;
	
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class PlaylistItem extends Sprite {
		
		private const _title:TextField = new TextField();
		private const _time:TextField = new TextField();
		private const format:TextFormat = new TextFormat("Tahoma", 18, 0x333333, false, false, false, null, null, TextFormatAlign.LEFT);
		private const timeFormat:TextFormat = new TextFormat("Tahoma", 18, 0x333333, false, false, false, null, null, TextFormatAlign.RIGHT);
		
		public const bar:Sprite = new Sprite();
		
		public var scale:Number = 1;
		public var src:String = undefined;
		public var length:Number = 0;
		private var file:Sound;
		
		private var barAlpha:Number = 0.3;
		private var barColor:uint = 0x000000;
		
		private var props:Object = {
			width: 300, height: 32, background:0xe0e0e0, alpha:1,
			offset: 32, fontSize: 18
		};
		
		private var defaultProps:Object = {
			width: 300, height: 32, background:0xe0e0e0, alpha:1,
			offset: 32, fontSize: 18
		};
		
		public function PlaylistItem(name:String, source:String, overrides:Object = null) {
			super();
			var rm:MovieClip;
			
			if (overrides) for (var i:String in overrides) {
				if(overrides.hasOwnProperty(i)) props[i] = overrides[i];
			}
			
			format.size = props.fontSize;
			
			_title.text = name;
			_title.height = props.fontSize * 1.5;
			_title.width = props.width;
			_title.selectable = false;
			_title.x = props.offset;
			_title.y = (props.height - _title.height) * 0.5;
			_title.setTextFormat(format);
			
			_time.text = "--:--";
			_time.width = 120;
			_time.x = props.width - 120 - 8;
			_time.y = 0;
			_time.selectable = false;
			_time.setTextFormat(timeFormat);
			
			rm = new MovieClip();
			rm.x = 16;
			rm.y = props.height / 2;
			rm.graphics.beginFill(0xFFFFFF, 0);
			rm.graphics.drawRect( -5, -5, 10, 10);
			rm.graphics.endFill();
			rm.graphics.lineStyle(3, 0, 0.35);
			rm.graphics.moveTo( -4, -4);
			rm.graphics.lineTo( 4, 4);
			rm.graphics.moveTo( 4, -4);
			rm.graphics.lineTo( -4, 4);
			rm.buttonMode = true;
			rm.addEventListener('click', remove);
			
			addChild(rm);
			addChild(_time);
			addChild(_title);
			
			file = new Sound();
			file.addEventListener(Event.COMPLETE, computeLength);
			file.load(new URLRequest(source));
			
			addEventListener(Event.ENTER_FRAME, function(e:Event):void {
				graphics.clear();
				graphics.beginFill(props.background, props.alpha);
				graphics.drawRect(0, 0, props.width, props.height);
			});
			
			addEventListener("mouseOver", function(e:Event):void {
				props.background = 0xFFFFFF;
				barAlpha = 1;
			});
			
			addEventListener("mouseOut", function(e:Event):void {
				props.background = defaultProps.background;
				barAlpha = 0.3;
			});
			
			bar.addEventListener(Event.ENTER_FRAME, function(e:Event):void {
				bar.graphics.clear();
				bar.graphics.beginFill(barColor, barAlpha);
				bar.graphics.moveTo(8, 0);
				bar.graphics.lineTo(0, -16);
				bar.graphics.lineTo(scale, -16);
				bar.graphics.lineTo(scale+8, 0);
				bar.graphics.lineTo(scale, 16);
				bar.graphics.lineTo(0, 16);
			});
			
			bar.addEventListener("mouseOver", function(e:Event):void {
				props.background = 0xFFFFFF;
				barAlpha = 1;
			});
			
			bar.addEventListener("mouseOut", function(e:Event):void {
				props.background = defaultProps.background;
				barAlpha = 0.3;
			});
		}
		
		private function remove(e:Event):void {
			Actuate.tween(this, 0.35, { alpha:0 }, true).onComplete(function():void {
				dispatchEvent(new Event('remove', true, true));
			} );
		}
		
		private function computeLength(e:Event):void {
			
			if(e.target.id3.songName) Actuate.tween(_title, 0.3, { alpha:0 } ).onComplete(function():void {
				_title.text = e.target.id3.songName;
				_title.setTextFormat(format);
				Actuate.tween(_title, 0.3, { alpha:1 } );
			});
			length = e.target.length;
			
			file.close();
			file = undefined;
			
			
			var seconds:String = "" + Math.floor((length / 1000) % 60);
			var minutes:String = "" + Math.floor(length / 1000 / 60);
			if (seconds.length < 2) seconds = "0" + seconds;
			_time.text = minutes + ":"+seconds;
			_time.setTextFormat(timeFormat);
			
			dispatchEvent(new Event('load', true, false));
		}
		
		public function set title(t:String):void {
			_title.text = t;
		}
		
		public function get title():String {
			return _title.text;
		}
	}
}