package  
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Vec2 extends Point {
		
		public var direction:Number, force:Number;
		
		public function Vec2(x:Number=0, y:Number=0, direction:Number = 0, force:Number = 0) {
			super(x, y);
			this.direction = direction;
			this.force = force;
		}
		
		public function update() {
			x += Math.cos(direction) * force;
			y += Math.sin(direction) * force;
		}
		
	}

}