package  
{
	import flash.events.Event;
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class MenuEvent extends Event {
		
		internal static const SELECT:String = "select";
		internal static const CANCEL:String = "cancel";
		internal static const DONE:String = "done";
		
		public var _data:Object;
		
		public function MenuEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
		}
		public function set data(d:Object):void {
			_data = d;
		}
		public function get data():Object {
			return _data;
		}
	}
}