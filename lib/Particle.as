package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Particle extends Point {
		
		public static const SENSOR:int = -1;
		public static const NONE:int = 0;
		public static const BULLET:int = 1;
		public static const LASER:int = 2;
		public static const EXPLOSION:int = 3;
		public static const FLAME:int = 4;
		public static const MD_BULLET:int = 5;
		public static const HURT:int = 6;
		
		
		public static const MD_BULLET_BMP:Vector.<uint> = [0xFF3366FF, 0xFF3366FF, 0xFF3366FF, 0xFF3366FF, 0xFF3366FF, 0xFFFF6633, 0xFFFF6633, 0xFF66FF33] as Vector.<uint>;
		
		private var _type:int = 0;
		public var color:uint = 0;
		public var vx:Number = 0;
		public var vy:Number = 0;
		public var lx:Number = 0;
		public var ly:Number = 0;
		private const r:Number = Math.random();
		public static var _angle:int = -45;
		public var rotation:Number = 0;
		public var speed:Number = 7;
		public var radius:Number = 1;
		public var alpha:Number = 1;
		public var callback:Function = null;
		public var data:Object = { };
		public var life:Number = 1;
		public var mod:Object = null;
		public var parent:Object = undefined;
		public var damage:Number = 100;
		
		public function Particle(type:uint = 0, x:Number=0, y:Number=0, c:uint = 0) {
			_type = type;
			switch(type){
				case 0 : break;
				case 1 : break;
				case 2 : break;
				case 3 : break;
				case 4 : color = 0x78FF6622; radius = Math.random() * 10; break;
				case 5 : color = 0xFF3366FF; radius = 1.5; break;
				case 6 : color = 0xFFFFFF00; radius = 0.5 + Math.random(); speed = 3 + Math.random() * 5;  break;
				default : break;
			}
			if (c) color = c;
			rotation = (_angle++)/180*Math.PI;
			super(x, y);
		}
		public function get type():uint {
			return _type;
		}
		public function set type(t:uint):void {
			switch(t) {
				case 0 : break;
				case 1 : break;
				case 2 : break;
				case 3 : break;
				case 4 : color = 0x78FF6622; radius = Math.random() * 10; break;
				case 5 : color = 0xFF3366FF; radius = 1.5; break;
				case 6 : color = 0xFFFFFF00; radius = 1.5 + Math.random() * 3; speed = 3 + Math.random() * 5;  break;
				default : break;
			}
			_type = t;
		}
		
		public function update():void {
			lx = x; ly = y;
			switch(_type) {
				case 0 : 
				case 1 : x += Math.cos(rotation) * speed; y += Math.sin(rotation) * speed; break;
				case 2 : break;
				case 3 : break;
				case 4 :
					x += Math.sin(rotation+Math.random()*Math.PI/2)*speed;
					y += Math.cos(rotation+Math.random()*Math.PI/2)*speed;
					break;
				case 5 : break;
				case 6 :
					x += Math.sin(rotation+Math.random()*Math.PI/2)*speed;
					y += Math.cos(rotation + Math.random() * Math.PI / 2) * speed;
					speed -= 0.035;
					alpha = Math.max(0, alpha - 1/15);
					break;
				default : break;
			}
			if (!!(callback)) callback(this);
		}
		
		public function draw(canvas:BitmapData):void {
			canvas.fillRect(new Rectangle(x-2, y-2, 4, 4), color);
		}
	}
}