package  
{
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	
	public class NeuralNet {
		
		private var net:Vector.<Vector.<Vector.<Number>>>;
		public function NeuralNet(i:uint, h:uint, o:uint, layers:uint = 1, weights:Vector.<Vector.<Vector.<Number>>> = undefined) {
			if (weights)
				net = weights;
			else {
				net = new Vector.<Vector.<Vector.<Number>>>();
				net.length = layers + 2;
				// for each layer...
				for (var l:uint = 0; l < net.length; l++) {
					// determine how many neurons it neads, and how many weights each neuron needs.
					var nn:uint, nw:uint;
					if (!l) // input neurons
						nn = nw = i;
					else if (l < layers - 1) // hidden layer neurons
						nn = h, nw = net[l - 1].length;
					else // output neurons
						nn = o, nw = net[l - 1].length;
					
					net[l] = new Vector.<Vector.<Number>>(); // create the layer
					net[l].length = nn;
					for (var ineuron:uint = 0; ineuron < nn; ineuron++ ) {
						net[l][ineuron] = new Vector.<Number>(); // crete the neuron
						net[l][ineuron].length = nw;
						for (var iweight:uint = 0; iweight < nw; iweight++) {
							net[l][ineuron][iweight] = Math.random()*2-1; // create the weight
						}
					}
				}
			}
		}
		
		public function process(args:Vector.<Number>):Vector.<Number> {
			if (args.length !== net[0][0].length) throw "NeuralNet.process(): argument count does not match inputs count";
			var inputs:Vector.<Number> = args;
			for (var l:uint = 0; l < net.length; l++) { // for each layer...
				var outputs:Vector.<Number> = new Vector.<Number>();
				var layer:Vector.<Vector.<Number>> = net[l];
				for (var n:uint = 0; n< layer.length; n++ ) { // ...neuron
					var neuron:Vector.<Number> = layer[n];
					var total:Number = 0;
					for (var w:uint = 0; w< neuron.length; total += inputs[w] * neuron[w++]){}; // ... and weight
					outputs.push(sigmoid(total, 1));
				}
				inputs = outputs;
			}
			return outputs;
		}
		
		public static function sigmoid(n:Number, w:Number):Number {
			return (1 / (1 + Math.exp( -n / w)));
		}
	}
}