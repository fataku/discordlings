package  
{
	import flash.display.BitmapData;
	import flash.display.BitmapDataChannel;
	import flash.filters.BlurFilter;
	import flash.filters.ColorMatrixFilter;
	import flash.filters.DisplacementMapFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Tower extends Point {
		
		
		
		public var radius:Number = 20;
		public var neighborRange:Number = 64;
		public var graphic:uint = 0;
		public var drag:Boolean = false;
		public var reputation:uint = 0;
		public var rotation:Number = 0;
		public var power:Number = 1;
		public var speed:Number = 0.03;
		public var mod:Object = null;
		public var _type:uint = 0;
		public var bmp:BitmapData;
		
		public var warmup:Number = 0.1;
		public var cooldown:Number = 0.03;
		public var falloff:Number = 0.2;
		public var range:Number = 2000;
		public var threshold:Number = 0.45;
		public var passthru:Boolean = false;
		
		private var maxTargets:uint;
		private var targets:Vector.<Object>;
		
		public var beams:Vector.<Number> = new Vector.<Number>();
		public var ends:Vector.<Number> = new Vector.<Number>();
		
		public var color:uint = 0xFFFFFFFF;
		
		public var from:Point = null; // primary source, i.e. the player.
		public var sources:Vector.<Object> = new Vector.<Object>(); // secondary source; i.e. other towers.
		public var effectsMatrix:Matrix = new Matrix();
		public var positionMatrix:Matrix = new Matrix();
		
		public static const radii:Vector.<Number> = new Vector.<Number>();
		public static const graphics:Vector.<uint> = new Vector.<uint>();
		public static const displace:BitmapData = new BitmapData(64, 1920, false, 0x7f7f7f);
		
		public function Tower(x:Number = 0, y:Number = 0, type:uint = 0 ) {
			super(x, y);
			//rotation = Math.PI / 6 * Math.floor(Math.random() * 6);
			color = Math.ceil(0xFFFFFF * Math.random()) + 0xFF000000;
			graphic = type;
			this.type = type;
			
			var beamCount:uint = Math.ceil(Math.random() * 3);
			if (beamCount < 2) passthru = true;
			
			for (var i:uint = 0; i < beamCount; i++) {
				beams.push(Math.floor(12*Math.random())*30);
				ends.push(range);
			}
			
			switch(type) {
				case 1: break;
				case 2: break;
				case 3: break;
				case 4: break;
				case 5: break;
				default: graphic = 0;
			}
			
			maxTargets = 1;
			targets = new Vector.<Object>();
			bmp = new BitmapData(32, 32, true, 0xFFFFFFFF);
			rotation = -Math.PI / 2;
			effectsMatrix.translate(x, y);
			effectsMatrix.rotate(this.rotation+Math.PI/2);
			positionMatrix.translate(x - bmp.width / 2, y - bmp.height / 2);
			displace.perlinNoise(32, 32, 8, Math.random() * 0xFFFFFFFF, true, true, 7, true);
			displace.applyFilter(displace, displace.rect, new Point(), new BlurFilter(64, 0, 3));
		}
		
		public function update():void {
			//if (drag) this.x = mouseX, this.y = mouseY;
			//other stuff
			effectsMatrix = new Matrix();
			
			if (this.targets.length) {
				var t:Object = targets[0] as Object;
				var r:Number = Math.atan2(t.y - this.y, t.x - this.x);
				this.rotation += Math.min(0.5, Math.max(-0.5,  r - this.rotation));
			}
			//effectsMatrix.rotate(-Math.PI);
			effectsMatrix.translate(-32, 0);
			effectsMatrix.rotate(this.rotation-Math.PI/2);
			effectsMatrix.translate(this.x, this.y);
			//targets[0].health-=1;
		}
		
		public function set type(t:uint):void {
			// graphic = graphics[t];
			// radius = radii[t];
			mod = {
				frame: 0,
				interpolation: "linear",
				increment: 1,
				stremgth: 32,
				step:function(prop:String, target:Object, params:Object = null):Number {
					params = params || mod;
					return target[prop] + params.strength * Math.sin(params.frame/180);
				}
			}
			_type = t;
			bmp = new BitmapData(32, 32, true, 0xFFFFFFFF);
			power = 1;
		}
		
		public function get type():uint {
			return _type;
		}
		
		public function hitTest(p:Object):Boolean {
			var target:Point = new Point(p.x, p.y);
			var dist:Number = Point.distance(target, this);
			var test:Point = new Point(x + Math.cos(rotation) * dist, y + Math.sin(rotation) * dist);
			
			return (Math.abs(target.x - test.x) < p.width / 2 && Math.abs(target.y - test.y) < p.height / 2);
		}
		
		public static function get effects():BitmapData {
			//var e:BitmapData = new BitmapData(64, 1920, true, 0);
			var f:BitmapData = new BitmapData(64, 1920, true, 0);
				f.fillRect(new Rectangle(30, 0, 4, 1920), 0xFF00FFFF);
				f.applyFilter(f, f.rect, new Point(), new BlurFilter(16, 0, 3));
				f.fillRect(new Rectangle(30, 0, 4, 1920), 0xFFEFFFFF);
			
			return f;
		}
		
		public function set target(t:Object):void {
			targets.unshift(t);
			targets.length = maxTargets;
		}
		
		public function draw(canvas:BitmapData):void {
			canvas.draw(bmp, positionMatrix);
		}
	}
}