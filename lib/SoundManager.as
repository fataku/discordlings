package  
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.SampleDataEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	import FFT2;
	import FFTElement;
	
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class SoundManager extends EventDispatcher{
		public static const SLIDE:String = "slide";
		public static const mx:Class = SoundMixer;
		public var sound:Sound;
		public var sc:SoundChannel;
		public var ba:ByteArray;
		public var ba_fft:ByteArray;
		public var effect:String = SLIDE;
		
		public var fade:Number = 0; // currently at;
		public var decay:Number = 0.1;
		public var fadeLength:Number = 3; //seconds;
		public var position:int = -1;
		public var samples:uint = 8192;
		public var speed:Number = 1;
		public var playing:Boolean = false;
		
		public var energy:Number = 0;
		
		public var playlist:Array = [];
		public var playlistAt:uint = 0;
		
		public var error:Error;
		
		public var paused:Boolean = false;
		
		private const sounds:Vector.<Sound> = new Vector.<Sound>();
		
		private var _spectrum:ByteArray = new ByteArray();
		
		private var fft:FFT2 = new FFT2();
		private var xRe:Vector.<Number> = new Vector.<Number>(512);
		private var xIm:Vector.<Number> = new Vector.<Number>(512);
		
		public function SoundManager(urls:Array = undefined) {
			super(this);
			
			fft.init(9);
			xIm.fixed = true;
			
			sound = new Sound();
			var sc:SoundChannel = new SoundChannel();
			ba = new ByteArray();
			SoundMixer.soundTransform = new SoundTransform(1, 0);
			
			if (urls && urls.length) {
				playlist = urls;
				load(playlist[0]);
			}
		}
		
		public function load(url:String):Boolean {
			position = 0;
			if (sounds.length && sounds[0] && sounds[0].hasEventListener('complete')) {
				sounds[0].removeEventListener("complete", begin);
			}
			sounds[0] = new Sound();
			sounds[0].addEventListener("complete", begin);
			sounds[0].load(new URLRequest(url));
			return true;
		}
		
		public function pause():void {
			if (!paused) {				
				paused = true;
				SoundMixer.stopAll();
				sc.stop();
			}else {
				sound.play();
				paused = false;
			}
		}
		
		public function get spectrum():ByteArray {
			/*var i:uint;
			for (i = 0; i < 512; i++) {
				xRe[i] = ba.readFloat();
				xIm[i] = 0;
			}
			
			fft.run(xRe, xIm, false);
			
			_spectrum.clear()
			const SCALE:Number = 1 / Math.LOG10E;
			for (i = 0; i < 512; i++) {
				_spectrum.writeFloat(xRe[i]);
			}
			
			return _spectrum;*/
			SoundMixer.computeSpectrum(_spectrum, true);
			return _spectrum;
		}
		
		private function sampleData(e:SampleDataEvent):void {
			ba.position = 0;
			var ii:uint = sounds[0].extract(ba, samples, position);
			if (position / 44.1 > sounds[0].length) {
				if (playlistAt < playlist.length - 1) {
					position = 0;
					load(playlist[++playlistAt]);
				}else{
					var ev:Event = new Event("sound_complete", false, false);
					dispatchEvent(ev);
					sc.stop();
					SoundMixer.stopAll();
					this.playing = false;
					return;
				}
			}
			distort(effect, fade);
			fade = Math.max(0, (fade - decay));
			e.data.writeBytes(ba);
			
			//SoundMixer.computeSpectrum(_spectrum, true);
			
			if (!playing) {
				dispatchEvent(new Event("start", false, false));
				playing = true;
			}
			position += samples;
		}
		
		private function begin(e:Event):void {
			if (sound.hasEventListener(SampleDataEvent.SAMPLE_DATA) === false) {
				sound.addEventListener(SampleDataEvent.SAMPLE_DATA, sampleData);
			}
			position = 0;
			sc = sc || sound.play();
		}
		
		public function distort(type:String = "random", amount:Number = 0):void {
			ba.position = 0;
			switch(type) {
				
				case "noise": default:
					var i:uint = 0;
					var hz:uint = 20;
					var fin:uint = 4096;
					
					if (amount > 0 ) while (ba.position < ba.length - 8) {
						var s:Number = ba.readFloat();
						var f:Number = s*(1-amount) + (Math.random()*2-1)*0.6*amount;
						//var amp:Number = (i/fin*Math.PI) * (i/fin*hz*Math.PI);
						//var f:Number = Math.sin(amp);
						//ba.position -= 8;
						ba.writeFloat(f);
						ba.writeFloat(f);
						i+=1;
					}
					break;
			}
			ba.position = 0;
		}
		
		public function butterfly(src:*):Array {
			return [];
		}
	}
}