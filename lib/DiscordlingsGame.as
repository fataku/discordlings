package  
{
	import com.eclecticdesignstudio.motion.Actuate;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.FocusDirection;
	import flash.display.MovieClip;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.HTMLUncaughtScriptExceptionEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.NativeDragEvent;
	import flash.events.SampleDataEvent;
	import flash.events.TimerEvent;
	import flash.filters.BitmapFilter;
	import flash.filters.BlurFilter;
	import flash.filters.ColorMatrixFilter;
	import flash.filters.DisplacementMapFilter;
	import flash.filters.DropShadowFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.ui.Mouse;
	import flash.utils.ByteArray;
	import flash.utils.setTimeout;
	import flash.ui.Keyboard;
	import flash.ui.KeyLocation;
	import flash.utils.Timer;
	
	import Assets;
	import Enemy;
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class DiscordlingsGame extends Game {
		
		public static const states:Object = {
			PREPARATION:0,
			DEFENCE:1,
			SCORE:2
		};
		
		public static const console:Object = {
			log:trace // because I keep thinking this is Javascript. I give up.
		}
		
		public var status:uint = states.PREPARATION;
		
		public var debug:Boolean = false;
		private const rd_roll:Array = [Assets.rd_bankLeft, Assets.rd_stable, Assets.rd_bankRight];
		
		private static const trailPiece:Vector.<uint> = new <uint>[0xFFEE4144, 0xFFF37033, 0xFFFDF6AF, 0xFF62BC4D, 0xFF1E98D3, 0xFF672F89];
		public static const TRAIL_MAX_LENGTH:uint = 32;
		
		private const RD:Point = new Point();
		private const RD_pose:Vector.<BitmapData> = new Vector.<BitmapData>(3);
		private const RDspeed:Number = 128;
		private const RDMaxSpeed:Number = 256;
		private var RDcanMove:Boolean = true;
		
		private var sfx:BitmapData;
		private var cancel:Array = [];
		
		private var enemies:Vector.<Enemy> = new Vector.<Enemy>();
		private var particles:Vector.<Particle> = new Vector.<Particle>();
		private const sprites:Vector.<Bitmap> = new Vector.<Bitmap>();
		private const bullets:Vector.<Rectangle> = new Vector.<Rectangle>();
		private const towers:Vector.<Tower> = new Vector.<Tower>();
		private const trail:Vector.<Point> = new Vector.<Point>(TRAIL_MAX_LENGTH);
		
		private var bg:Bitmap = new Assets.ForestStage();
		private var base:Base;
		
		private var trailFade:Number = 1;
		private const booms:Vector.<Rectangle> = new Vector.<Rectangle>();
		private var snd:SoundManager;
		private var snd_bmp:BitmapData;
		public var weapon:DubstepLaser = new DubstepLaser();
		public var rainbowTrail:Trail = new Trail();
		private const towerMenu:Sprite = new Sprite();
		private var rbCircle:Sprite = new Sprite();
		
		private var firing:Boolean = false;
		private var useFFT:Boolean = false;
		
		private var towerRange:Number = 64 + 20;
		private var towerShields:Sprite = new Sprite();
		
		private var c:uint = 0xFFFFFFFF;
		
		private const debugLayer:MovieClip = new MovieClip();
		private const debugLayer2:MovieClip = new MovieClip();
		
		public var playlist:Array = [];
		
		private var elapsed:Number = 0;
		public var totalLength:Number = 0;
		private const arc:Number = Math.PI * 2 / 6.78;
		private const arcStart:Number = -arc/2 + Math.PI*0.001;
		private const arcStep:Number = Math.PI / 7200;
		
		private var runtime:Timer;
		private var beat:Boolean = false;
		private var currentBeat:uint = 0;
		private var lastBeat:uint;
		private var beatSpeed:Number = 60000 / 120; // bpm
		private var difficulty:uint = 1; // 1: full beats, 2: half-beats, 4: quarter-beats, 8: eighth-beats, 16: sixteenth-beats
		
		private const pauseScreen:Sprite = new Sprite();
		private var pausing:Boolean = false;
		
		public var hurt:uint = 0; // milliseconds of pain
		public var lives:uint = 3; // number of hits before recharge
		
		public function DiscordlingsGame(width:uint, height:uint, cont:DisplayObject=null, fps:uint=30) {
			var i:uint;
			super(width, height, cont, fps);
			
			base = new Base(width / 2, height + Assets.rdBase.height / 2 - 100, Assets.rdBase.width, Assets.rdBase.height);
			
			pauseScreen.graphics.beginFill(0x787878, 0.35);
			pauseScreen.graphics.drawRect(0, 0, 1920, 1080);
			pauseScreen.visible = false;
			
			//sfx = new BitmapData(width, height, true, 0x00FFFFFF);
			// set all trail indeces to empty points
			for (i = 0; i < trail.length; trail[i++] = new Point(width/2, height+256)) { };
			
			towerMenu.graphics.lineStyle(0.3, 0xe7e7e7, 0.85);
			towerMenu.graphics.beginFill(0xe7e7e7, 0.85);			
			
			RD.y = height + 128;
			RD.x = width / 2;
			
			var hardpoints:uint = 6;
			var arc:Number = (2 * Math.PI / hardpoints);
			var spread:Number = 40; //120
			
			for (i = 0; i < hardpoints; i++) {
				if (!i) towerMenu.graphics.moveTo(Math.cos(arc * -1 - Math.PI / 2 * 0) * spread, Math.sin(arc * -1 - Math.PI / 2 * 0) * spread);
				var tower:Sprite = new Sprite();
					tower.x = Math.cos(arc * i - Math.PI / 2*0) * spread;// + camera.rect.width / 2;
					tower.y = Math.sin(arc * i - Math.PI / 2*0) * spread;// + camera.rect.height / 2;
					tower.graphics.beginFill(0xe7e7e7, 1);
					tower.graphics.drawCircle(0, 0, 16);
					tower.graphics.endFill();
					tower.name = "tower-" + (i + 1);
				towerMenu.graphics.lineTo(Math.cos(arc * i - Math.PI / 2 * 0) * spread, Math.sin(arc * i - Math.PI / 2 * 0) * spread);
				towerMenu.addChild(tower);
			}
			
			towerMenu.graphics.endFill();
			towerMenu.y = (camera.height - towerMenu.height) / 2;
			towerMenu.x = (camera.width - towerMenu.width) / 2
			towerMenu.filters = [new DropShadowFilter(4, 90, 0x333333, 0.65, 16, 16, 1)];
			towerMenu.addEventListener(MouseEvent.CLICK, pickTower);
			
			this.addChild(towerMenu);
			
			addEventListener(MouseEvent.MOUSE_DOWN, mouseInput);
			addEventListener(MouseEvent.MOUSE_UP, mouseInput);
			addEventListener(MouseEvent.RIGHT_CLICK, mouseInput);
			addEventListener(MouseEvent.MOUSE_WHEEL, mouseInput);
			addEventListener(MouseEvent.MIDDLE_CLICK, mouseInput);
			addEventListener(MouseEvent.MOUSE_MOVE, mouseInput);
			
			this.addChild(debugLayer);
			this.addChild(debugLayer2);
			weapon.debugLayer = debugLayer;
			
			addChild(pauseScreen);
		}
		
		override public function start(e:Event = null):void {
			super.start(e);
			sprites.push(bg);
			bg.x = (width - bg.width) / 2;
			runtime = new Timer(beatSpeed / difficulty, 0);
			runtime.addEventListener(TimerEvent.TIMER, function():void { beat = false; } );
			runtime.start();
		}
		
		override public function stop(e:Event = null):void {
			super.stop(e);
			if(towerMenu.parent) towerMenu.parent.removeChild(towerMenu);
			SoundMixer.stopAll();
			Mouse.show();
		}
		
		override public function pause(e:Event = null):void {
			if (!pausing) {
				pausing = true;
				super.pause();
				if (snd) snd.pause();
				
				pauseScreen.visible = true;
				pauseScreen.alpha = paused?0:1;
				Actuate.tween(pauseScreen, 0.3, { alpha:1-pauseScreen.alpha } ).onComplete(function():void{
					pauseScreen.visible = paused;
					pausing = false;
				});
			}
		}
		
		override public function draw(...args):void {
			var basePos:Matrix, bars:Sprite, i:uint;
			
			if (paused) { Mouse.show(); return; }
			camera.fillRect(camera.rect, 0xFF000000);
			if (!snd_bmp) snd_bmp = new BitmapData(2, 256);
			
			
			// Background & Base ============================================
			camera.draw(bg);
			
			basePos = new Matrix();
			basePos.translate(stage.stageWidth/2-Assets.rdBase.width/2, stage.stageHeight-100);
			
			
			// progress bars (hp & time)
			bars = new Sprite();
			bars.graphics.beginFill(0xE0E0E0, 1);
			bars.graphics.drawCircle(Assets.rdBase.width / 2, Assets.rdBase.height / 2, 430);
			bars.graphics.endFill();
			
			bars.graphics.moveTo(Assets.rdBase.width / 2, Assets.rdBase.height / 2);
			bars.graphics.beginFill(0xFF00FF, 1);
			for (i = 0; i*arcStep < arc * base.hp / base.maxhp; i++ ) {
				bars.graphics.lineTo(
					Math.cos(arcStart+i*arcStep-Math.PI / 2)*430+Assets.rdBase.width / 2,
					Math.sin(arcStart+i*arcStep-Math.PI / 2)*430+Assets.rdBase.height / 2
				);
			}
			bars.graphics.endFill();
			
			bars.graphics.beginFill(0xE0E0E0, 1);
			bars.graphics.drawCircle(Assets.rdBase.width / 2, Assets.rdBase.height / 2, 420);
			bars.graphics.endFill();
			
			bars.graphics.moveTo(Assets.rdBase.width / 2, Assets.rdBase.height / 2);
			bars.graphics.beginFill(0xFFFF00, 1);
			for (i = 0; i*arcStep < arc; i++ ) {
				bars.graphics.lineTo(
					Math.cos(arcStart+i*arcStep-Math.PI / 2)*420+Assets.rdBase.width / 2,
					Math.sin(arcStart+i*arcStep-Math.PI / 2)*420+Assets.rdBase.height / 2
				);
			}
			bars.graphics.endFill();
			
			camera.draw(bars, basePos);
			camera.draw(Assets.rdBase, basePos);
			
			towerShields.graphics.clear();
			
			
			// Actors =======================================================
			if (state === states.DEFENCE) rainbowTrail.draw(camera);
			
			
			// Towers =======================================================
			var step:uint = weapon.realHeight;
			var eff:BitmapData = Tower.effects;
			
			towerShields.graphics.beginFill(0x0078FF);
			towerShields.graphics.drawCircle(width/2, height + Assets.rdBase.height / 2 - 100, Assets.rdBase.height / 2 + towerRange);
			towerShields.graphics.endFill();
			
			for (var ti:uint = 0; ti < towers.length; ti++) {
				var t:Tower = towers[ti] as Tower;
				towerShields.graphics.beginFill(0x0078FF);
				towerShields.graphics.drawCircle(t.x, t.y, towerRange-32);
				towerShields.graphics.endFill();
				t.draw(camera); // camera.draw(t.bmp, t.positionMatrix);
				if(state === states.DEFENCE) camera.draw(eff, t.effectsMatrix, new ColorTransform(1, 1, 1, snd.sc.leftPeak));
			}
			
			
			// Enemies =======================================================
			for (var e:uint = 0; e < enemies.length; e++) {
				enemies[e].draw(camera);
			}
			
			// Weapon Bullets =======================================================
			if(firing && RDcanMove) weapon.draw(camera);
			
			
			// Rainbow Dash =======================================================
			var roll:uint = (hurt)?1:1 + Math.max( -1, Math.min(1, Math.round((mouseX - trail[1].x)>>4)));
			if(state === states.DEFENCE) camera.draw(rd_roll[roll], new Matrix(1, 0, 0, 1, RD.x - 32, RD.y - 16), new ColorTransform(1, 1, 1, Math.cos(hurt/frametime)));
			
			
			// particles ====================================================
			var mat:Matrix = new Matrix();
			var defaultParticle:BitmapData = new BitmapData(1, 1, true, 0);
			for (var part:uint = 0; part < particles.length; part++) {
				var p:Particle = particles[part];
				if(p){
					if (p.radius <= 1 && p.alpha > 0 && p.type !== Particle.MD_BULLET) {
						camera.setPixel32(p.x, p.y, p.color);
						
					}else{
						if (p.type === Particle.MD_BULLET) {
							var bullet:Bitmap = Assets.bullet;
							var sx:Number = ((p.radius / 100 * 4)) * 1.5 * ((p.data.index%2)*2-1),
								sy:Number = weapon.height;
								
								mat.identity();
								mat.scale(sx, sy);
								mat.translate( -sx*bullet.width/2 + p.x, p.y);
								
							camera.draw(bullet, mat, new ColorTransform(1, 1, 1, p.alpha));//*/
							//camera.fillRect(new Rectangle(p.x - 2, p.y, p.radius * 2, 5), p.color - 0x78000000);
							
						}else {
							defaultParticle.floodFill(0, 0, p.color);
							mat.identity();
							mat.scale(p.radius*2, p.radius*2);
							mat.translate(p.x - p.radius, p.y - p.radius);
							camera.draw(defaultParticle, mat, new ColorTransform(1, 1, 1, p.alpha) );
						}
					}
				}
			}
			
			
			// boom displancement map(s) ====================================
			for (i = 0; i < booms.length; i++) {
				var d:BitmapData, dm:Matrix;
				if (!booms[i]) continue;
				
				var boom:Rectangle = booms[i];
					boom.height += boom.width;
				var scale:Number = boom.height/256;
				var prop:Number = boom.width;
				//a = 1 - boom.height / camera.rect.height * 0.5;
				var al:Number = 1 - boom.height / (camera.rect.height >> 1);
				trace("Boom with scale:", scale, "; alpha:", al , "camera height:", camera.rect.height);
				dm = new Matrix();
				dm.scale(scale, scale);
				
				d = new BitmapData(256 * scale, 256 * scale, false, 0);
				d.draw(Assets.displacer, dm);
				
				camera.applyFilter(camera, camera.rect, new Point(), new DisplacementMapFilter(d, new Point(boom.x - 128 * scale, boom.y - 128 * scale), 1, 2, 128*al, 128*al));
			}
			
			// UI Elements? ==================================================
			if (state === states.DEFENCE){
				Mouse.hide()
				var c_pos:Matrix = new Matrix();
					c_pos.translate(mouseX-Assets.cursor.width/2, mouseY-Assets.cursor.height/2);
				camera.draw(Assets.cursor, c_pos);
			}
		}
		
		override public function resolve(...args):void {
			if(!paused && state === states.DEFENCE){
				var last:Point = new Point(RD.x, RD.y);
				var diff:Point = new Point(last.x - mouseX, last.y - mouseY);
				
				hurt = Math.max(0, hurt - frametime);
				
				// RD Position ======================================================
				if(RDcanMove && !hurt){
					if(!firing){
						RD.x -= Math.cos(Math.atan2(RD.y-mouseY, RD.x - mouseX)) * Math.min(RDMaxSpeed, Math.abs(RD.x - mouseX));
						RD.y -= Math.sin(Math.atan2(RD.y-mouseY, RD.x - mouseX)) * Math.min(RDMaxSpeed, Math.abs(RD.y - mouseY));
					}else {
						RD.x -= Math.cos(Math.atan2(RD.y-mouseY, RD.x - mouseX)) * Math.min(RDspeed, Math.abs(RD.x - mouseX));
						RD.y -= Math.sin(Math.atan2(RD.y-mouseY, RD.x - mouseX)) * Math.min(RDspeed, Math.abs(RD.y - mouseY));
					}
					weapon.firing = firing;
				}else weapon.firing = false;
				
				// Trail ======================================================
				trail.unshift(new Point(RD.x, RD.y));
				trail.length = Math.min(trail.length, TRAIL_MAX_LENGTH);
				
				
				if (snd && snd.playing) weapon.wubs = (useFFT)?snd.spectrum:snd.ba;
				weapon.trail = rainbowTrail.trail = trail;
				weapon.update();
				
				
				// Enemies - Spawn ======================================================
				
				var i:uint = 0;
				if (!beat) {
					beat = true;
					currentBeat++;
					
					if (!(currentBeat % 2)) {
						enemies.push(new Enemy(Enemy.BASIC));
					}
					if (!(currentBeat % 4)) {
						enemies.push(new Enemy(Enemy.GUARD));
					}
					if (!(currentBeat % 8)) {
						enemies.push(new Enemy(Enemy.CURTAIN_LEFT));
						enemies.push(new Enemy(Enemy.CURTAIN_RIGHT));
					}
					if (!(currentBeat % 16)) {
						enemies.push(new Enemy(Enemy.SCISOR_LEFT));
						enemies.push(new Enemy(Enemy.SCISOR_RIGHT));
					}
					
					
					while(++i < 3) { // spawn small fries
						var enemy:Enemy = new Enemy(Enemy.BASIC);
							enemy.color = Math.random() * 0xFFFFFF + 0xFF000000;
						enemies.push(enemy);
					}
				}
				
				
				// Enemies - Update ======================================================
				enemies = enemies.filter(function(obj:Enemy, i:uint, all:*):Boolean {
					var burst:uint = 0, p:Particle;
					
					// check for weapon collisions
					if (weapon.collide(obj)) obj.damage(weapon.damage), burst++;
					if (rainbowTrail.collide(obj)) obj.damage(rainbowTrail.damage), burst++;
					if (obj.health <= 0) return false;
					
					for (i = 0; i < towers.length; i++ ) {
						if (towers[i].hitTest(obj)) {
							obj.damage(towers[i].power);
							burst++;
						}
					}
					// check particle collisions
					for (i = 0; i < particles.length; i++) {
						p = particles[i];
						
						if (!p.parent || p.parent !== RD) continue;
						
						if (obj.x-obj.width/2 < p.x + p.radius && obj.x+obj.width/2 > p.x-p.radius
						 && obj.y - obj.height / 2 < p.y + p.radius && obj.y + obj.width / 2 > p.y - p.radius) {
							 
							obj.damage(p.damage);
							burst++;
						}
					}
					//unload bullets
					while (obj.bullets.length) particles.push(obj.bullets.pop());
					
					// create vfx
					for (i = 0; i<burst;i++) {
						p = new Particle(Particle.HURT, obj.x, obj.y, undefined);
						p.speed = 4 + 7 * Math.random();
						p.radius = Math.ceil(Math.random() * 6);
						p.rotation = Math.PI*2*Math.random();
						particles.push(p);
					}
					
					for (i = 0; i < towers.length; i++ ) {
						if (towers[i].hitTest(obj)) obj.damage(towers[i].power);
					}
					
					if (obj.y - obj.height > camera.height) return false;
					else if (obj.health > 0) {
						obj.data[0] = RD.x - obj.x;
						obj.data[1] = RD.y - obj.y;
						obj.behave();
					}
					return obj.health > 0;
				});
				base.hp-=0.1;
				
				
				// Particles ======================================================
				particles = particles.filter(function(p:Particle, i:uint, all:Vector.<Particle> ):Boolean {
					if (p.life === 0 ||  p.x < - 64 || p.y < - 64 || p.x > camera.rect.width + 64 || p.y > camera.rect.height + 64 || p.alpha <= 0) {
						return false;
					} else p.update();
					if (!hurt && p.parent && p.parent !== RD && Math.abs(p.x - RD.x) < p.radius + 10 && Math.abs(p.y - RD.y) < p.radius + 10) {
						hurt = 1000;
						return false;
					}
					if (p.type === Particle.BULLET && rainbowTrail.collide(p)) return false;
					return true;
				});
				
				
				// Towers ======================================================
				for (var ti:uint = 0; ti < towers.length; ti++) {
					var t:Tower = towers[ti] as Tower;
					if(enemies.length){
						enemies.sort(function(a:Object, b:Object):int {
							var da:Number = Point.distance(new Point(a.x, a.y), t as Point);
							var db:Number = Point.distance(new Point(b.x, b.y), t as Point);
							return (da === db)?0:(da<db)?-1:1;
						} );
						t.target = enemies[0];
					}
					t.update();
				}
				
				
				// Sanic Booms ======================================================
				for (i = 0; i < booms.length; i++) {
					if (!booms[i]) continue;
					var boom:Rectangle = booms[i];
						boom.height += boom.width;
					if (1 - boom.height / (camera.rect.height >> 1) <= 0) booms[i] = undefined;
				}
			}
		}
		
		override public function keyboardInput(e:KeyboardEvent):void {
			
			var dn:Boolean = e.type === "keyDown";
			switch(e.keyCode) {
				case 38: //trace("up");
					break;
				case 37: //trace("left");
					break;
				case 39: //trace("right");
					break;
				case 40: //trace("down");
					break;
				case 32: //trace("space");
					particles.length = 0;
					booms.length = 0;
					//if(dn) snd.effect = SoundManager.SLIDE, snd.fade = 1;
					break;
				case Keyboard.B : if (e.type === KeyboardEvent.KEY_DOWN) debug = !debug; break;
				case Keyboard.ESCAPE : if (e.type === KeyboardEvent.KEY_DOWN) cancel.length?cancel.shift()():pause(); break;
				case Keyboard.F : if (e.type === KeyboardEvent.KEY_DOWN) useFFT = !useFFT; break;
				case Keyboard.P : if (e.type === KeyboardEvent.KEY_DOWN) state = states.DEFENCE; break;
				default: trace(e.keyCode); break;
			}
		}
		
		private function get state():uint {
			return status;
		}
		
		private function set state(s:uint):void {
			//status = s;
			if(s === states.DEFENCE){
				snd = new SoundManager(playlist);
				snd.addEventListener("sound_complete", stop);
				snd.addEventListener("start", function():void {
					status = s;
					if(towerMenu.parent) towerMenu.parent.removeChild(towerMenu);
				});
			}else if (s === states.PREPARATION || s === states.SCORE) {
				snd.removeEventListener("sound_complete", stop);
				SoundMixer.stopAll();
			}
		}
		
		private function mouseInput(e:MouseEvent):void {
			switch(e.type) {
				case "click" : break;
				case "rightClick" :
					if(RDcanMove){
						var force:Number = 14, diameter:Number = 0, angleOff:Number = Math.random()*Math.PI*2;
						for (var i:uint = 1440; i > 0; i--) {
							var p:Particle = new Particle(Particle.FLAME, RD.x, RD.y, getARGB((i%360), 1, 0.55));
								p.speed = force;
								p.radius = Math.ceil(Math.random() * 6);
								p.rotation += angleOff;
								p.parent = "player";
							particles.push(p);
						}
						booms.push(new Rectangle( RD.x, RD.y, force, diameter));
						trace("Booms: ", booms.length);
						firing = RDcanMove = false;
						setTimeout(function():void { RD.x = mouseX; RDcanMove = true; }, 2000);
						
						trail.unshift(new Point(RD.x, RD.y));
						trail.unshift(new Point(RD.x, RD.y-256));
						RD.y = camera.height+64;
						trail.unshift(new Point(RD.x, -32));
						trail.unshift(new Point( -32, -32));
						trail.unshift(new Point( -32, camera.height + 32));
						trail.unshift(new Point(RD.x, RD.y-16));
						trail.unshift(new Point(RD.x, RD.y));
						}
					break;
				case "mouseDown" :  firing = true; break;
				case "mouseUp" : firing = false; weapon.i = 0;  break;
				case "mouseMove" : break;
				default: trace(e.type); break;
			}
		}
		
		private function pickTower(e:MouseEvent = null):void {
			//var h:uint = towerMenu.getChildIndex(e.target as  DisplayObject)/towerMenu.numChildren*360;
			var hardpoint:Sprite = new Sprite();
				hardpoint.graphics.lineStyle(3, 0xdedede, 1);
				hardpoint.graphics.beginFill(0xFFFFFF, 0.85);
				hardpoint.graphics.drawCircle(0, 0, 20);
				hardpoint.startDrag(true, camera.rect);
			this.addChild(hardpoint);
			
			hardpoint.addEventListener(MouseEvent.MOUSE_MOVE, moveHardpoint, false, 0, true );
			hardpoint.addEventListener(MouseEvent.CLICK, placeTower, false, 0, true);
			
			cancel.unshift(function():void {
				hardpoint.stopDrag();
				hardpoint.removeEventListener(MouseEvent.CLICK, placeTower);
				hardpoint.removeEventListener(MouseEvent.MOUSE_MOVE, moveHardpoint);
				removeChild(hardpoint);
			});
		}

		private function chainTower(t:Tower):void {
			/*var lastRotation:Number = t.rotation % (Math.PI*2);
			//t.rotation += t.power * t.speed;
			t.rotation = t.rotation % (Math.PI * 2);
			
			for (var i:uint = 0; i < towers.length; i++ ) {
				var ti:Tower = towers[i] as Tower;
				if (t === ti) continue;
				var rad:Number = Math.atan2(ti.y - t.y, ti.x - t.x);
				for (var m:uint = 0; m < t.beams.length; m++) {
					var d:Number = Math.min(Point.distance(ti as Point, t as Point), t.power*t.range, t.ends[m]);
					var test:Point = new Point(t.x+Math.cos(t.rotation+(t.beams[m]/180*Math.PI))*d, t.y+Math.sin(t.rotation+(t.beams[m]/180*Math.PI))*d);
					if(debug) camera.fillRect(new Rectangle(test.x - 2, test.y - 2, 4, 4), 0xFF00FFFF);
					if (test.x <= ti.x + ti.radius &&
						test.x >= ti.x - ti.radius &&
						test.y <= ti.y + ti.radius &&
						test.y >= ti.y - ti.radius) {
						if(!ti.passthru) t.ends[m] = d;
						if(t.power > ti.threshold) {
							ti.power = Math.max(ti.power, t.power - t.falloff);
						}
					}
				}
			}*/
		}
		
		private function moveHardpoint(e:Event):void {
			var h:Sprite = e.target as Sprite;
				h.x = mouseX; h.y = mouseY;
			var p:Vector.<Tower> = towers.slice();
			if (p && p.length > 1) p.sort(function(a:Object, b:Object):int {
				var da:Number = Point.distance(new Point(a.x, a.y), new Point(h.x, h.y));
				var db:Number = Point.distance(new Point(b.x, b.y), new Point(h.x, h.y));
				if (da !== db) { return (da < db)? -1 : 1; }
				return 0;
			});
			
			var b:Point = new Point(mouseX, mouseY);
			if (p && p.length > 0) {
				var a:Point = new Point(p[0].x, p[0].y);
				if (Point.distance(a, b) <= p[0].radius + towerRange || Point.distance(b, new Point(base.x, base.y)) <= Assets.rdBase.height / 2 + 32 + towerRange) {
					h.alpha = 1;
				}else h.alpha = 0.3;
			}else {
				if(Point.distance(b, new Point(base.x, base.y)) <= Assets.rdBase.height / 2 + 32 + towerRange){
					h.alpha = 1;
				}else h.alpha = 0.66;
			}
		}
			
		private function placeTower(e:MouseEvent = null):void {
			var ok:Boolean = false;
			var h:Sprite = e.target as Sprite;
				h.x = mouseX; h.y = mouseY;
			var p:Vector.<Tower> = towers.slice();
			if (p && p.length > 1) p.sort(function(a:Object, b:Object):int {
				var da:Number = Point.distance(new Point(a.x, a.y), new Point(h.x, h.y));
				var db:Number = Point.distance(new Point(b.x, b.y), new Point(h.x, h.y));
				if (da !== db) { return (da < db)? -1 : 1; }
				return 0;
			});
			
			var b:Point = new Point(mouseX, mouseY);
			if (p && p.length > 0) {
				var a:Point = new Point(p[0].x, p[0].y);
				if (Point.distance(a, b) <= p[0].radius + towerRange || Point.distance(b, new Point(base.x, base.y)) <= Assets.rdBase.height / 2 + 32 + towerRange) {
					ok = true;
				}
			}else {
				if(Point.distance(b, new Point(base.x, base.y)) <= Assets.rdBase.height / 2 + 32 + towerRange){
					ok = true;
				}
			}
			
			if(ok){
				e.target.stopDrag();
				e.target.removeEventListener(MouseEvent.CLICK, placeTower);
				e.target.removeEventListener(MouseEvent.MOUSE_MOVE, moveHardpoint);
				e.target.parent.removeChild(e.target);
				//this.removeEventListener(MouseEvent.MOUSE_MOVE, moveHardpoint);
				towers.push(new Tower(e.target.x, e.target.y, 0));
				cancel.shift();
				
			}
		}
		
		public static function getARGB(h:Number, s:Number = 1, l:Number = 0.5):uint {
			// Code courtesy of http://snipplr.com/view/34817/
			h = h / 360;
			var r:Number;
			var g:Number;
			var b:Number;
			 
			if(l==0) r=g=b=0;
			else{
				if(s == 0) r=g=b=l;
				else {
					var t2:Number = (l<=0.5)? l*(1+s):l+s-(l*s);
					var t1:Number = 2*l-t2;
					var t3:Vector.<Number> = new Vector.<Number>();
						t3.push(h+1/3);
						t3.push(h);
						t3.push(h-1/3);
					var clr:Vector.<Number> = new Vector.<Number>();
						clr.push(0);
						clr.push(0);
						clr.push(0);
					for(var i:int=0;i<3;i++)
					{
						if(t3[i]<0) t3[i]+=1;
						if(t3[i]>1) t3[i]-=1;
						 
						if(6*t3[i] < 1) clr[i]=t1+(t2-t1)*t3[i]*6;
						else if(2*t3[i]<1) clr[i]=t2;
						else if(3*t3[i]<2) clr[i]=(t1+(t2-t1)*((2/3)-t3[i])*6);
						else clr[i]=t1;
					}
					r=clr[0], g=clr[1], b=clr[2];
				}
			}
			//return { r:int(r * 255), g:int(g * 255), b:int(b * 255) };
			return 0xFF000000 + uint(0xFF*r) * 0x10000 + uint(0xFF*g) * 0x100 + uint(0xFF*b);
		}
	}
}