package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display3D.textures.Texture;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import flash.desktop.NativeDragManager;
	import flash.desktop.NativeDragActions;
	import flash.desktop.NativeDragOptions;
	import flash.desktop.NativeWindowIcon;
	import flash.events.NativeDragEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.filesystem.FileMode;
	import flash.desktop.ClipboardFormats;
	
	import com.eclecticdesignstudio.motion.Actuate;
	import com.eclecticdesignstudio.motion.easing.Elastic;
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class MainMenu extends Menu {
		
		public static var playlistBarWidth:Number = 1420;
		
		private const btn_play:Bitmap = new Assets.UIPlay();
		private const btn_continue:Bitmap = new Assets.UIContinue();
		private const btn_options:Bitmap = new Assets.UIOptions();
		
		public var options:Menu;
		public var stageSelect:Menu;
		public var characterSelect:Menu;
		
		private var weapon_buttons:Array;
		private var pressStart:Boolean = true;
	
		public var playlistLength:Number = 0;
		
		private const goFilters_on:Array = [new DropShadowFilter(4, 180, 0, 0.15, 0, 0, 1, 1), new GlowFilter(0xFFFF00, 1, 20, 4, 1, 3)];
		private const goFilters_off:Array = [goFilters_on[0]];
		private const ready_button:MovieClip = new Assets.Start();
		private const playlist_display:MovieClip = new MovieClip();
		
		
		private const gameMenu:Sprite = new Sprite();
		private const newgame:Sprite = new Sprite();
		private const freeplay:Sprite = new Sprite();
		private const gameoptions:Sprite = new Sprite();
		
		
		public var playlist:Array = [];
		
		public var startBar:MovieClip = new MovieClip();
		
		public function MainMenu(w:Number = undefined, h:Number = undefined) {
			super();
			
			graphics.beginFill(0, 0);
			graphics.drawRect(0, 0, 1920, 1080);
			
			options = new Menu();
			stageSelect = new Menu();
			characterSelect = new Menu();
			
			//characterSelect.set_bg(Assets.characterSelect, null);
			
			characterSelect.hide();
			this.addChild(characterSelect);
			
			this.background.bitmapData = new BitmapData(1920, 1080, false, 0xFFFFFFFF);
			set_bg(Assets.disc, new Matrix(1, 0, 0, 1, 0, 0));
			
			ready_button.getChildAt(0).x -= ready_button.width/2;
			ready_button.getChildAt(0).y -= ready_button.height/2;
			ready_button.x = 1920 - ready_button.width / 2 - 8;
			ready_button.y = ready_button.height/2;
			ready_button.alpha = 0.85;
			ready_button.filters = goFilters_off;
			
			weapon_buttons = new Array();
			
			setup();
		}
		
		private function setup():void {
		
			startBar.y = 1080 - 120;
			startBar.addChild(ready_button);
			
			playlist_display.y = startBar.y - 100 - 730;
			var playlist_mask:Sprite = new Sprite();
				playlist_mask.graphics.beginFill(0xFFFFFF);
				playlist_mask.graphics.drawRect(0, 0, 1300, 1080-120);
				playlist_mask.graphics.endFill();
			playlist_display.mask = playlist_mask;
			
			for (var i:uint = 0, weapons:Array = ['a', 'b', 'c', 'd'], colors:Array = [0xFF0000, 0xFFFF00, 0x00FFFF, 0x00FF00]; i < 4; i++) {
				var weapon_button:Sprite = new Sprite();
					weapon_button.graphics.beginFill(colors[i], 2 );
					weapon_button.graphics.drawRect(0, 0, 100, 830);
					weapon_button.graphics.endFill();
					weapon_button.x = 1921;
					weapon_button.y = startBar.y - 100 - 730;
					weapon_button.alpha = 0.65;
					weapon_button.name = weapons[i];
					
					weapon_button.addEventListener('mouseOver', weapon_mouseHandler, false,0, true);
					weapon_button.addEventListener('mouseOut', weapon_mouseHandler, false,0, true);
					weapon_button.addEventListener('click', weapon_mouseHandler, false,0, true);
					
					weapon_buttons.push(weapon_button);
					
					characterSelect.addChild(weapon_button);
			}
			
			var listFormat:TextFormat = new TextFormat('Tahoma', 32, 0xFFFFFF, false, false, false, null, null, TextFormatAlign.CENTER);
			var newgameText:TextField = new TextField();
				newgameText.text = "New Game";
				newgameText.setTextFormat(listFormat);
				newgameText.selectable = false;
				newgameText.width = 320;
				newgame.buttonMode = true;
				newgame.addChild(newgameText);
			var freeplayText:TextField = new TextField();
				freeplayText.text = "Free Play";
				freeplayText.setTextFormat(listFormat);
				freeplayText.selectable = false;
				freeplayText.width = 320;
				freeplay.y = 56;
				freeplay.buttonMode = true;
				freeplay.addChild(freeplayText);
			var gameoptionsText:TextField = new TextField();
				gameoptionsText.text = "Options";
				gameoptionsText.setTextFormat(listFormat);
				gameoptionsText.selectable = false;
				gameoptionsText.width = 320;
				gameoptions.buttonMode = true;
				gameoptions.y = 112;
				gameoptions.addChild(gameoptionsText);
			
			gameMenu.addChild(gameoptions);
			gameMenu.addChild(freeplay);
			gameMenu.addChild(newgame);
			gameMenu.alpha = 0;
			gameMenu.x = 0.5 * (1920 - gameMenu.width);
			gameMenu.y = 800;
			gameMenu.filters = [new GlowFilter(0, 1, 8, 8, 3, 3)];
			addChild(gameMenu);
			
			this.addEventListener(MouseEvent.CLICK, initialClick);
			startBar.addEventListener(Event.ENTER_FRAME, drawStartBar );
		}
		
		private function initialClick(e:Event):void {
			
			trace(e.target, e.target === newgame.getChildAt(0));
			
			if (pressStart) {
				Actuate.tween(gameMenu, 0.75, { alpha: 1 } );
				pressStart = false;
				
			}else if (e.target === newgame.getChildAt(0)) {
				
				Actuate.tween(gameMenu, 0.5, { alpha: 0 } );
				Actuate.tween(startBar, 0.5, { alpha:1 } );
				Actuate.tween(background, 0.5, { alpha: 0 } );
				
				var goFormat:TextFormat = new TextFormat('Geo Sans Light', 32, 0xFFFFFF);
				var goText:TextField = new TextField(); 
					goText.height = 48;
					goText.width = ready_button.width;
					goText.x = -24; goText.y = -goText.height / 2;
					goText.htmlText = "<h1>Ready!</h1>";
					goText.selectable = false;
					goText.setTextFormat(goFormat);
					goText.filters = [new DropShadowFilter(2, 90, 0x999999, 0.83, 1, 1, 1, 3)];
				pressStart = false;
				characterSelect.show();
				
				ready_button.x = -ready_button.width; ready_button.y = 0;
				ready_button.addEventListener('mouseOver', start_mouseHandler, false,0, true);
				ready_button.addEventListener('mouseOut', start_mouseHandler, false,0, true);							
				ready_button.addEventListener('click', start_mouseHandler, false, 0, true);
				
				characterSelect.addChild(playlist_display);
				ready_button.addChild(goText);
				characterSelect.addChild(startBar);
				
				addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, handle_drag);
				addEventListener(NativeDragEvent.NATIVE_DRAG_DROP, handle_drag);
				
				playlist_display.addEventListener('remove', reposition);
				playlist_display.addEventListener('load', reposition);
				playlist_display.addEventListener(MouseEvent.MOUSE_WHEEL, scroll);
				
				weapon_buttons.forEach(function(wb:Object, i:uint, all:Array):void {
					Actuate.tween(wb, 0.35, {x: 1820-i*100}).delay(i/10);
				});
				
				dispatchEvent(new MenuEvent(MenuEvent.SELECT));
			}
		}
		
		private function checkReady():void {
			
			if (playlist.length < 1) return;
			
			var toX:Number = 1920 - ready_button.width / 2 - 8;
			ready_button.y = ready_button.height/2;
			
			Actuate.tween(ready_button, 0.75, { x:toX } );
		}
		
		private function start_mouseHandler(e:MouseEvent):void {
			switch(e.type) {
				case "mouseOver" :
					Actuate.tween(ready_button, 0.3, { scaleX: 1.05, scaleY:1.05} );
					e.currentTarget.filters = goFilters_on;
					break;
				case "mouseOut":
					Actuate.tween(ready_button, 0.3, { scaleX:1, scaleY: 1} );
					e.currentTarget.filters = goFilters_off;
					break;
				case "click" :
					dispatchEvent(new MenuEvent(MenuEvent.DONE));
					Actuate.tween(ready_button, 0.3, {x:1920 + ready_button.width} );
					ready_button.removeEventListener('mouseOver', start_mouseHandler);
					ready_button.removeEventListener('mouseOut', start_mouseHandler);							
					ready_button.removeEventListener('click', start_mouseHandler);
					break;
			}
		}
		
		private function weapon_mouseHandler(e:MouseEvent):void {
			switch(e.type) {
				case "mouseOver" :
					Actuate.tween(e.currentTarget, 0.3, { alpha: 1}, false );
					break;
				case "mouseOut":
					Actuate.tween(e.currentTarget, 0.3, { alpha: 0.65}, false );
					break;
				case "click" :
					var current:int = -1;
					dispatchEvent(new MenuEvent(MenuEvent.SELECT));
					
					weapon_buttons.forEach(function(obj:Object, i:uint, all:Array):void {
						if (obj !== e.currentTarget){
							var toX:Number = 1820 - i * 100;
							if (current >= 0) toX -= 220;
							Actuate.tween(obj, 0.35, { x: toX, scaleX:1 }, true);
						}else if (obj === e.currentTarget) current = i;
					});
					
					e.currentTarget.alpha = 0.5;
					Actuate.tween(e.currentTarget, 0.35, { x: 1820- current*100 - 220, alpha: 1, scaleX: 3.2 }, true );
					break;
			}
		}
		
		private function handle_drag(e:NativeDragEvent):void {
			switch(e.type) {
				case NativeDragEvent.NATIVE_DRAG_ENTER :
					NativeDragManager.acceptDragDrop(this);
					break;
				case NativeDragEvent.NATIVE_DRAG_DROP : 
					(e.clipboard.getData(ClipboardFormats.FILE_LIST_FORMAT) as Array).forEach(addFile)
					checkReady();
					break;
			}
		
		}
		
		private function addFile(f:File, index:uint = undefined, all:Array = undefined):void {
			switch (f.extension.toLowerCase()) {
				case "mp3": case "wav": case "ogg": case "wma":
					var item:PlaylistItem = new PlaylistItem(f.name, f.nativePath, {width: 1300});
						item.x = -item.width/4;
						item.y = item.height * playlist.length;
						item.alpha = 0;
						item.bar.y = startBar.height/2
						item.bar.alpha = 0;
					startBar.addChild(item.bar);
					Actuate.tween(item, 0.75, { x: 0, alpha:1 }, true).delay(0.035*index);
					Actuate.tween(item.bar, 0.75, { alpha:1 }, true).delay(0.035*index);
					playlist_display.addChild(item);
					playlist.push(f.nativePath);
			}
		}
		
		private function reposition(e:Event = null):void {
			
			if (e.type === "remove") {
				
				var which:uint = e.target.parent.getChildIndex(e.target);
				e.target.parent.removeChild(e.target);
				Actuate.tween(e.target.bar, 0.3,  { alpha:0 } ).onComplete(function():void {
					startBar.removeChild(e.target.bar);
				} );
				
				playlist = playlist.slice(0, which).concat(playlist.slice(which + 1));
				
			}
			
			if (playlist.length <= 0) Actuate.tween(ready_button, 0.75, { x:stage.stageWidth + 320 } ).onComplete(function():void {
				ready_button.x = -320;
			});
			
			playlistLength = 0;
			for (var i:uint = 0, ii:uint = playlist_display.numChildren; i < ii; i++) {
				var item:PlaylistItem = playlist_display.getChildAt(i) as PlaylistItem;
				Actuate.tween(item, 0.15, { y:32 * i }, true);
				playlistLength += item.length;
			}
			
			var xindex:Number = 100;
			for (i = 0, ii = playlist_display.numChildren; i < ii; i++) {
				item = playlist_display.getChildAt(i) as PlaylistItem;
				var toScale:Number = item.length / playlistLength * playlistBarWidth - 1;
				item.scale = e.type==="load"?toScale:0;
				Actuate.tween(item, 0.35, { scale:toScale } );
				item.bar.x = xindex;
				item.bar.y = startBar.height/2
				
				Actuate.tween(item.bar, 0.3, { alpha:1 } );
				
				xindex += toScale + 1;
			}
			
			
		}
		
		private function scroll(e:MouseEvent):void {
			var to:Number = Math.min(130, Math.max( -playlist_display.height + 130, playlist_display.y + e.delta * 32));
			Actuate.tween(playlist_display, 0.75, {y:to}, false);
		}
		
		private function drawStartBar(e:Event):void {
				
			var h:Number = (ready_button.height / ready_button.scaleY) - 1;
			startBar.graphics.clear();
			startBar.graphics.lineStyle(3, 0xaaaaaa, 0.65);
			startBar.graphics.beginFill(0xaaaaaa, 1);
			startBar.graphics.drawRect(0, 0, 1920, h);
			startBar.graphics.endFill();
			
			startBar.graphics.lineStyle(3, 0xEFEFEF, 0.65);
			startBar.graphics.beginFill(0xFFFFFF, 0.35);
			startBar.graphics.moveTo(108, 0 + h/2 + 1);
			startBar.graphics.lineTo(100, -16 + h/2 + 1);
			startBar.graphics.lineTo(100 + playlistBarWidth, -16 + h/2 + 1);
			startBar.graphics.lineTo(100 + playlistBarWidth+8, 0 + h/2 + 1);
			startBar.graphics.lineTo(100 + playlistBarWidth, 16 + h/2 + 1);
			startBar.graphics.lineTo(100, 16 + h/2 + 1);
			
		}
	}
}