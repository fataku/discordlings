package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import com.eclecticdesignstudio.motion.Actuate;
	
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Menu extends MovieClip{
		
		
		public const background:Bitmap = new Bitmap();
		public const buttons:Sprite = new Sprite();
		public var rect:Rectangle = new Rectangle();
		
		public const cases:Array = new Array();
		
		public function Menu() {
			super();
			this.addChild(buttons);
			buttons.addEventListener(MouseEvent.CLICK, clickHandler);
		}
		
		private function clickHandler(e:Event):void {
			if (e.target !== buttons) {
				if (cases[e.target.name] !== undefined) {
					cases[e.target.name](e)
					
					var me:MenuEvent = new MenuEvent(MenuEvent.SELECT);
						me.data = e.target;
					
					this.dispatchEvent(me);
				};
			}
		}
		
		public function set_bg(bg:Bitmap, matrix:Matrix = null, ct:ColorTransform = null):MovieClip {
			if (!background.bitmapData) background.bitmapData = bg.bitmapData.clone();
			else background.bitmapData.draw(bg, matrix, ct);
			if (!background.parent) this.addChildAt(background, 0);
			return this;
		}
		
		public function add_buttons(buttons:Vector.<DisplayObject>, callbacks:Vector.<Function>):MovieClip {
			var l:uint = buttons.length;
			for (var i:uint = 0; i < l; i++) {
				var b:DisplayObject = buttons[i] as DisplayObject; this.addChild(b);
				if (callbacks[i]) b.addEventListener(MouseEvent.CLICK, callbacks[i]);
			}
			return this;
		}
		
		public function add_button(btn:DisplayObject, callback:Function):DisplayObject {
			var button:DisplayObject = btn as DisplayObject;
				button.name = cases.length.toString();
			
			buttons.addChild(button);
			cases[button.name] = callback;
			
			return this;
		}
		
		public function remove_button(btn:DisplayObject):MovieClip {
			var success:Boolean;
			try {
				var button:DisplayObject = btn as DisplayObject;
				buttons.removeChild(buttons.getChildByName(button.name.toString()));
				cases[button.name] = undefined;
			}catch (e:Error) {
				trace(e);
			}finally{
				return this;
			}
		}
		
		public function hide(time:Number = 0):MovieClip {
			if (time === 0) this.alpha = 0, this.visible = false;
			else {
				var t:Menu = this;
				Actuate.tween(t, time, { alpha:0 } ).onComplete(function(t:Menu):void { t.visible = false; }, t);
			}
			return this;
		}
		
		public function show(time:Number = 0):MovieClip {
			if (time === 0) this.alpha = 1, this.visible = true;
			else {
				this.alpha = 0; this.visible = true;
				Actuate.tween(this, time, { alpha:1 } );
			}
			return this;
		}
		
		public function toggle():MovieClip {
			this.alpha = 1 - this.alpha;
			this.visible = !this.visible;
			return this;
		}
	}
}