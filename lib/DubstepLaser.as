package
{
	import flash.display.BitmapData;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import Assets;
	//import flash.display.MovieClip;
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class DubstepLaser extends Weapon{

		public var wubs:ByteArray;
		public var trail:Vector.<Point>;
		public var defaultWidth:uint = 3;
		public var firing:Boolean = false;

		public function DubstepLaser() {
			super();
			damage = 100;
		}

		override public function draw(canvas:BitmapData = undefined):void {
			if (!canvas) {
				throw "DubstepLaser.draw(): no BitmapData provided to draw on";
				return;
			}

			if (!trail || !trail.length) return;

			for (var ii:uint = 0; ii < trail.length - 1 && ii * step < i; ii++ ) {

				var from:Point = new Point();
					from.copyFrom(trail[ii]);
				var to:Point = new Point();
					to.copyFrom(trail[ii + 1]);
					
				from.y -= (ii * step);
				to.y -= ((ii + 1) * step);
					
				var dis:Number = Math.abs(from.y - to.y);
				var dif:Number = from.x - to.x;
				var amount:Number = dis / realHeight;

				var power:Number = 1;
				var bm:Matrix = new Matrix();
				
				for (var bi:uint = 0; bi <= amount; bi++) {
					if (wubs && wubs.bytesAvailable < 16)
						wubs.position = 0;

					power = (wubs && wubs.bytesAvailable)?(wubs.readFloat()):1;
					bm.identity();
					bm.translate( -width / 2, 0);
					bm.scale(width * power, height/2);
					bm.translate(from.x - bi * (dif/amount) + 15, from.y -(1+bi) * realHeight);
					canvas.draw(Assets.bullet, bm, new ColorTransform(1, 1, 1, 5));

					power = (wubs && wubs.bytesAvailable)?(wubs.readFloat()):1;
					bm.identity();
					bm.translate( -width / 2, 0);
					bm.scale(-width * power, height/2);
					bm.translate(from.x - bi * (dif/amount) - 15, from.y - (1+bi) * realHeight);
					canvas.draw(Assets.bullet, bm, new ColorTransform(1, 1, 1, 1));
				}
			}
			return;
		}

		override public function update():void {
			if (this.debugLayer) this.debugLayer.graphics.clear();
			if(firing) this.i += step;
			else this.i = 0;
			return;
		}

		override public function collide(target:Object):Boolean {
			if (trail) for (var piece:uint = 0; piece < trail.length - 1 && piece * step < i; piece++ ) {
				
				var from:Point = new Point();
					from.copyFrom(trail[piece]);
				var to:Point = new Point();
					to.copyFrom(trail[piece + 1]);
					
				from.y -= piece * step;
				to.y -= (piece + 1) * step;
				
				if (target.y < to.y || target.y > from.y+step
				 || target.x +(target.width||0)/2 < Math.min(from.x, to.x) - 24
				 || target.x -(target.width||0)/2 > Math.max(from.x, to.x) + 24)
					continue;
				
				var offset:Number;
				if(from.x - to.x !== 0) offset = (to.y - from.y) / (to.x - from.x) * (target.x - from.x);
				else offset = 0;
				if (offset === 0 || target.y - target.speed <= from.y + step + offset && target.y >= from.y + offset) {
					return true;
				}
			}
			return false;
		}
	}
}