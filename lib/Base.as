package  
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Base extends Rectangle {
		
		public var maxhp:Number = 1000.00;
		public var hp:Number = 1000.00;
		
		public var shields:Object = {hp: 100.00, range: 64, fill:null};
		public var graphic:BitmapData;
		
		public function Base (x:Number = 0, y:Number = 0, width:Number=0, height:Number=0) {
			super(x, y, width, height);
		}
	}
}