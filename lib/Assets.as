package  
{
	import flash.display.Bitmap;
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Assets {
		[Embed(source = "../misc/snd_bullet.png")]
		public static const Bullet:Class;
		
		[Embed(source="../misc/sprites/RD_bankLeft_64.png")]
		public	static const RDLeft:Class;
		
		[Embed(source="../misc/sprites/RD_bankRight_64.png")]
		public	static const RDRight:Class;
		
		[Embed(source="../misc/sprites/RD_stable_top_64.png")]
		public	static const RDTop:Class;
		
		[Embed(source="../misc/bgs/Basic-Stage.png")]
		public static const ForestStage:Class;
		
		[Embed(source = "../misc/Displace.png")]
		public static const Displacer:Class;
		
		[Embed(source = "../misc/cursor.png")]
		public static const Cursor:Class;
		
		[Embed(source = "../misc/characterSelect.png")]
		public static const CharacterSelect:Class;
		
		[Embed(source="../misc/Discordlings_disc2.png")]
		public static const Disc:Class;
		[Embed(source="../misc/sprites/base_full.png")]
		public static const RDBase:Class;
		[Embed(source="../misc/Discordlings_ui-play.png")]
		public static const UIPlay:Class;
		[Embed(source="../misc/Discordlings_ui-continue.png")]
		public static const UIContinue:Class;
		[Embed(source="../misc/Discordlings_ui-options.png")]
		public static const UIOptions:Class;
		
		[Embed(source="../misc/buttons/start.swf")]
		public static const Start:Class;
		
		[Embed(source="../misc/ShieldsPattern.png")]
		public static const ShieldsPattern:Class;
		
		public static const disc:Bitmap = new Disc;
		public static const uiPlay:Bitmap = new UIPlay;
		public static const uiContinue:Bitmap = new UIContinue;
		public static const uiOptions:Bitmap = new UIOptions;
		
		public static const characterSelect:Bitmap = new CharacterSelect();
		public static const displacer:Bitmap = new Displacer();
		public static const rd_stable:Bitmap = new RDTop();
		public static const rd_bankRight:Bitmap = new RDRight();
		public static const rd_bankLeft:Bitmap = new RDLeft();
		public static const bullet:Bitmap = new Bullet();
		public static const cursor:Bitmap = new Cursor();
		public static const shieldPattern:Bitmap = new ShieldsPattern();
		public static const rdBase:Bitmap = new RDBase();
	}

}