package
{
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import Assets;
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Trail extends Weapon {

		private static const trailPiece:Vector.<uint> = new <uint>[0xFFEE4144, 0xFFF37033, 0xFFFDF6AF, 0xFF62BC4D, 0xFF1E98D3, 0xFF672F89];
		public static const TRAIL_MAX_LENGTH:uint = 32;



		private const template:Sprite = new Sprite();
		private var trailFade:Number = 1;
		public var trail:Vector.<Point> = new Vector.<Point>(TRAIL_MAX_LENGTH);

		public function Trail() {
			super();
			damage = 100 / trailPiece.length;
		}

		override public function draw(canvas:BitmapData = undefined):void {
			template.graphics.clear();
			for (var tt:int = 0; tt < trail.length; tt++ ) {
				var pa:Point = trail[tt] as Point;
				if (!trail[tt]) continue;
				if (tt < trail.length - 2) {
					for (var c:uint = 0; c < trailPiece.length; c++){

						var pos:Number = ((c+0.5) - trailPiece.length / 2) * 4;
						var wid:uint = 2;

						var pb:Point = trail[tt + 1];
						var p0:Point = new Point(pa.x - pb.x, pa.y - pb.y);
						var d0:Number = Math.sqrt(p0.x * p0.x + p0.y * p0.y);
						var angle0:Number = Math.atan2(p0.y, p0.x) + Math.PI / 2;

						var pc:Point = trail[tt + 2];
						var angle1:Number = Math.atan2(pb.y - pc.y, pb.x - pc.x) + Math.PI / 2;

						var alpha0:Number = trailFade;

						template.graphics.beginFill(trailPiece[c], Math.min(1 - (tt + 1) / trail.length, alpha0 ) );

						template.graphics.moveTo(pa.x + Math.cos(angle0) * (pos + wid), pa.y + Math.sin(angle0) * (pos + wid));
						template.graphics.lineTo(pa.x + Math.cos(angle0) * (pos - wid), pa.y + Math.sin(angle0) * (pos - wid));

						template.graphics.lineTo(pb.x + Math.cos(angle1) * (pos - wid), pb.y + Math.sin(angle1) * (pos - wid));
						template.graphics.lineTo(pb.x + Math.cos(angle1) * (pos + wid), pb.y + Math.sin(angle1) * (pos + wid));

						template.graphics.endFill();
					}
				};
			}
			canvas.draw(template, new Matrix(1, 0, 0, 1, 0, 6 ), null, "normal", null, true);
		}

		override public function update():void {

		}
		
		override public function collide(target:Object):Boolean{
			for (var i:uint = 0; i < trail.length-1; i++ ) {
				var from:Point = trail[i] || undefined, to:Point = trail[i + 1] || undefined;

				if ((!from || !to)
				 || (Math.min(from.x, to.x)-10 > target.x)
				 || (Math.min(from.y, to.y)-10 > target.y)
				 || (Math.max(from.x, to.x)+10 < target.x)
				 || (Math.max(from.x, to.x)+10 < target.x)
				) continue;
				
				var a1:Number = Math.atan2(to.y - from.y, to.x - from.x);
				var a2:Number = Math.atan2(target.y - from.y, target.x - from.x);

				var d:Number = Math.sqrt(Math.pow( target.x - from.x, 2) + Math.pow( target.y - from.y, 2));

				var comparator:Point = new Point(from.x + Math.cos(a1) * d, from.y + Math.sin(a1) * d);
				var test:Point = new Point(from.x + Math.cos(a2) * d, from.y + Math.sin(a2) * d);
				var dist:Number = Point.distance(comparator, test);

				if (dist <= trailPiece.length * 2 + target.speed) {
					return true;
				}
			}
			return false;
		}
	}

}