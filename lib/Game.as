package
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Matrix;
	import flash.utils.setTimeout;
	import flash.utils.Timer;
	import flash.utils.setTimeout;
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Game extends Sprite {
		
		internal static const START:String = "start";
		internal static const PAUSE:String = "pause";
		internal static const UNPAUSE:String = "unpause";
		internal static const STOP:String = "stop"; // stopped before ending
		internal static const HALT:String = "halt"; // stopped unexpectedly... will it ever happen in a way that allows an event to be generated? We'll see...
		internal static const RESIZE:String = "resize"; // window size has changed
		internal static const COMPLETE:String = "complete"; // end of stage has been attained
		internal static const UPDATE:String = Event.ENTER_FRAME; // update loop begins
		internal static const PRE_RENDER:String = Event.PREPARING; // draw loop begins
		internal static const POST_RENDER:String = Event.RENDER; // draw loop about to end
		
		internal static var MAX_FRAMESKIP:uint = 5;
		
		public var run:Boolean = false;
		public var paused:Boolean = false;
		public var screen:Bitmap;
		public var camera:BitmapData;
		
		public var frametime:uint = 33;
		private var initTime:Date = new Date();
		private var loops:uint = 0;
		private var skipped:uint = 0;
		//public var container:DisplayObject;
		
		public function Game(width:uint, height:uint, cont:DisplayObject = null, fps:uint=30) {
			super();
			camera = new BitmapData(width, height, true, 0);
			screen = new Bitmap(camera, "auto", true);
			addChild(screen);
			frametime = fps;
		}
		public function set_camera(c:BitmapData):void {
			camera = c;
			screen.bitmapData = camera;
		}

		public function start(e:Event = null):void {
			run = true;
			initTime = new Date();
			update();
			dispatchEvent(new Event(START));
		}
		
		public function pause(e:Event = null):void {
			paused = !paused;
			dispatchEvent(new Event(paused?PAUSE:UNPAUSE));
		}
		
		public function stop(e:Event = null):void {
			run = false;
			dispatchEvent(new Event(STOP));
		}
		
		public function update():void {
			var now:Date = new Date();
			var a:uint = now.getMilliseconds();
			
			var current:int = (now.getMilliseconds() + now.getSeconds() * 1000 + now.getMinutes() * 60000 + now.getHours() * 3600000)
							- (initTime.getMilliseconds() + initTime.getSeconds() * 1000 + initTime.getMinutes() * 60000 + initTime.getHours() * 3600000);
			if (current < 0) {
				initTime = new Date();
				now = new Date();
				loops = 0;
				current = (now.getMilliseconds() + now.getSeconds() * 1000 + now.getMinutes() * 60000 + now.getHours() * 3600000)
						- (initTime.getMilliseconds() + initTime.getSeconds() * 1000 + initTime.getMinutes() * 60000 + initTime.getHours() * 3600000);
			}
			
			var ideal:uint = (loops++) * frametime;
			var diff:int = current-ideal;
			var skip:Boolean = diff > (frametime) && skipped < MAX_FRAMESKIP; //if we're already more than a frame behind, skip rendering this frame unless we've already skipped MAX_FRAMESKIP frames
			
			if (run) {
				// note: enemies are stored in a vector as a rect. use x and y to draw the appropriate graphic, w for it's type and h for it's state.
				
				//1:	place character to mouse
				//2:	resolve position of enemies
				//2.1:	resolve position of own bullets
				//2.2:	resolve position of enemy bullets
				//3: 	test all collisions
				//4:	spawn enemies if necessary.
				//----- drawing and handling happen asynchronously,
				//-----(there should be less calculations than frames every second,
				//----- to lighten load. use extrapolation for drawing movement.)
				if(!skip) skipped = 0, draw();
				else skipped++;
				resolve();
				var b:uint = (new Date()).getMilliseconds(); while (b < a) b += 1000;
				setTimeout(update, Math.max(0, frametime - diff - (b-a)));
			}
		}
		
		public function resolve(...args):void{
			
		};
		
		public function keyboardInput(e:KeyboardEvent):void {
			
		};
		
		public function draw(...args):void {
			camera.fillRect(camera.rect, 0);
			//-----
			for (var r:uint = 0; r < 270; r++) {
				var rx:Number = Math.sin((r+loops) / 180 * Math.PI) * 250 + camera.rect.width/2;
				var ry:Number = -Math.cos((r+loops) / 180 * Math.PI) * 250 + camera.rect.height/2;
				camera.setPixel32(rx+0.5, ry+0.5, 0xFFFFFFFF);
			}
			trace("If you see this message, you haven't set up \"override public function draw(...args):void{} yet.\"");
		}
	}
}