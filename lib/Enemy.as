package  
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import Vec2;
	
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Enemy extends Rectangle {
		
		public static const BASIC:int = 0;
		public static const CURTAIN_LEFT:int = 1;
		public static const CURTAIN_RIGHT:int = 2;
		public static const SCISOR_LEFT:int = 3;
		public static const SCISOR_RIGHT:int = 4;
		public static const GUARD:int = 5;
		
		public var maxHealth:Number = 100;
		public var health:Number = 100;
		public var _type:int = 0;
		public var color:uint = 0x00000000;
		
		public var dr:Number = 1; // defense ratio : lower = takes less damage.
		
		public var rotation:Number = Math.PI / 2;
		public var aim:Number = rotation;
		public var speed:Number = 4;
		
		private var tick:uint = 0;
		
		public const bullets:Vector.<Particle> = new Vector.<Particle>();
		
		//private var net:NeuralNet = new NeuralNet(4, 4, 4, 2);
		public const data:Vector.<Number> = new <Number>[0, 0, 0, 0];
		
		public function Enemy(t:uint = 0) {
			this.type = t;
			super(x, y, 16, 16);
		}
		
		public function set type(t:int):void {
			maxHealth = 100 + 25 * t;
			health = Math.min(maxHealth, health);
			
			var flip:Boolean = Math.random() > 0.5;
			switch(t) {
				case BASIC: x = flip? -32:1952; y = 350; rotation = flip? 0 : Math.PI;  break;
				case CURTAIN_LEFT: x = -32; y = 64; rotation = 0; break;
				case CURTAIN_RIGHT: x = 1952; y = 64; rotation = Math.PI; break;
				case SCISOR_LEFT: x = -32; y = 64; rotation = 0; break;
				case SCISOR_RIGHT: x = 1952; y = 64; rotation = Math.PI; break;
				case GUARD: break;
			}
			
			_type = t;
		}
		
		public function get type():int {
			return _type;
		}
		
		public function behave():void {
			
			if (this.health <= 0) return;
			
			if (!(tick % 60)) shoot();
			if(!this.type)this.rotation = Math.sin(tick / 30) + Math.PI / 2;
			
			this.x += speed * Math.cos(this.rotation);
			this.y += speed * Math.sin(this.rotation);
			
			/*for (var i:uint = 0; i < bullets.length; i++) {
				bullets[i].update();
			}*/
			
			tick++;
			if ((!(tick % 15) && type === Enemy.BASIC)
			 || (!(tick % 5) && (type === Enemy.CURTAIN_LEFT || type === Enemy.CURTAIN_RIGHT))
			 || (!(tick % 5) && (type === Enemy.SCISOR_LEFT  || type === Enemy.SCISOR_RIGHT))) shoot();
			
		}
		
		public function draw(canvas:BitmapData):void {
			canvas.fillRect(new Rectangle(x - width / 2, y - height / 2, width, height), color);
		}
		
		public function shoot():void {
			var bullet:Particle = new Particle(Particle.BULLET, x, y, 0xFFFF0000);
				bullet.speed = 5;
				bullet.radius = 2;
				bullet.parent = this;
			switch(type) {
				case Enemy.BASIC : bullet.rotation = rotation; break; // Math.atan2(data[1], data[0]);
				case Enemy.CURTAIN_LEFT: case Enemy.CURTAIN_RIGHT: 
				case Enemy.SCISOR_LEFT: case Enemy.SCISOR_RIGHT: bullet.rotation = Math.PI / 2; break;
			}
			bullets.push(bullet);
		}
		
		public function damage(n:Number):void {
			health -= n * dr;
		}
	}
}