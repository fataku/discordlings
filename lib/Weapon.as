package
{
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	//import flash.display.MovieClip;
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Weapon extends Object{
		public var height:uint = 5;
		public var width:uint = 8;
		public var spacing:uint = 2;
		public var realHeight:uint = 5;
		public var rof:uint = 16;
		public var step:uint = 7 * 16;
		public var i:uint = 0;
		public var debugLayer:MovieClip = undefined;
		public var damage:Number = 0; // per tick
		private var owner:Object;

		public function Weapon() {
			super();
		}

		public function draw(canvas:BitmapData = undefined):void {
			if (!canvas){
				throw "Weapon.draw(): no BitmapData provided to draw on";
				return;
			}
			return;
		}

		public function update():void {
			return;
		}

		public function collide(target:Object):Boolean {
			return false;
		}

		public function attachTo(target:Object):void {
			if (target.hasOwnProperty("x") && target.hasOwnProperty("y")) {
				owner = target;
			}else throw "weapon target must posess coordinate properties";
		}

		public function get attachedTo():Object {
			return owner;
		}

		public function set attachedTo(target:Object):void {
			attachTo(target);
		}
	}
}