package  
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.*;
	import flash.events.EventDispatcher;
	/**
	 * ...
	 * @author Dmitri Kolytchev
	 */
	public class Intro extends Menu{
		public var done:Boolean = false;
		private var timerId:uint = 0;
		public function Intro() {
			super();
			this.graphics.beginFill(0xFFFFFF, 1);
			this.graphics.drawRect(0, 0, 1920, 1080);
			this.addEventListener(MouseEvent.CLICK, complete);
		}
		override public function play():void {
			timerId = setTimeout(complete, 5000);
		}
		public function complete(e:MouseEvent = null):void {
			clearTimeout(timerId);
			dispatchEvent(new Event('complete'));
			done = true;
		}
	}
}